//#include "main.h"
//#include "fsm_garaje.h"
//#include "hw.h"


#include <stdint.h>
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

//Defino las entradas por teclado
#define EXIT            27  // ASCII para la tecla Esc 
#define FICHA  		    49  // ASCII para la tecla 1    EX SENSOR_1
#define CAFE		    50  // ASCII para la tecla 2    EX SENSOR_2
#define TE			    51  // ASCII para la tecla 3

//Defino tiempos de espera mas cortos que los enunciados para acortar simulacion
#define ESPERA_30       70      // 7 segundos
#define ESPERA_45       100     // 10 seundos
#define ESPERA_2        20      // 2 segundos

//Defino Variables booleanas
bool evFicha_On;
bool evTe_On;
bool evCafe_On;
bool evTick1seg;
uint8_t count_seg;

// Inicializo las Variables booleanas
static void clearEvents(void)
{
    evFicha_On = 0;
    evTe_On = 0;
    evCafe_On = 0;
    evTick1seg = 0;
  
}


//Declaro funciones
uint8_t hw_LeerEntrada(void);
void hw_Init(void);
void hw_DeInit(void);
void hw_Pausems(uint16_t t);
void esperando_orden(void);
void bienvenido (void);
void EgresoFicha (void);
void HacerTe (void);
void HacerCafe (void);

void HacerTe_Off (void);
void HacerCafe_Off (void);
void Led1Hz (void);
void Led5Hz (void);
void alarma (void);
void FinAlarma (void);


//Defino los estados
typedef enum {
    REPOSO,
    ESPERANDO,
    HACIENDO_CAFE,
    HACIENDO_TE,
    FINALIZANDO
} ESTADOS_CAFETERA;

ESTADOS_CAFETERA state;


struct termios oldt, newt; // esta en hw.c en src


// Configuraciones previas
void hw_Init(void) {
    // Configurar la terminal para evitar presionar Enter usando getchar()
    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);       
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);

    // Non-blocking input
    fcntl(STDIN_FILENO, F_SETFL, O_NONBLOCK);
}
 
void hw_DeInit(void) {
    // Restaurar la configuracion original de la terminal
    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);    
}

void hw_Pausems(uint16_t t) {
    usleep(t * 1000);
}

uint8_t hw_LeerEntrada(void) {
    return getchar();
}

int main(void)
{
    // uint8_t input;
        uint8_t input = 0;
    	uint16_t cont_ms = 0;

    hw_Init();

    bienvenido();
    Led1Hz();
    

    // Superloop hasta que se presione la tecla Esc
    while (input != EXIT) {

     
        input = hw_LeerEntrada();

        if (input == FICHA && evFicha_On == 0) {
            evFicha_On = 1;            
        }

        else if (input == TE && evTe_On == 0){
            evTe_On = 1;
        }

        else if (input == CAFE && evCafe_On == 0) {
            evCafe_On = 1;
        }

        cont_ms++;
        if (cont_ms == 10000) {
            cont_ms = 0;
            evTick1seg = 1;
        }

        
		switch (state) {
        case REPOSO:
            
            if (evFicha_On) {
                    count_seg=0; 
                    //evTick1seg = 1;
                    clearEvents();
                    evTick1seg = 1;
                    esperando_orden(); 
                    state = ESPERANDO;
            }
            break;

            
        case ESPERANDO:
       
         if (evTick1seg && (count_seg < ESPERA_30)){
             count_seg++;
         }
         else if (evTick1seg && (count_seg == ESPERA_30)) {
                    count_seg = 0;
            	    EgresoFicha (); 
                    bienvenido();
                    Led1Hz ();               
                    state = REPOSO;
            }

            if (evTe_On){     
          		    count_seg = 0; 
                    evFicha_On = 0;
                    HacerTe ();
                    Led5Hz ();
                    state = HACIENDO_TE;
                 }
           if (evCafe_On){
         		   count_seg= 0 ;
                   evFicha_On = 0;
                   HacerCafe ();
                   Led5Hz ();
                   state = HACIENDO_CAFE;           
           }
    
            evFicha_On = 0;


            break;

        case HACIENDO_TE:
         		

         		if (evTe_On && (count_seg < ESPERA_30)) {
                    count_seg++;
                 }
                else if (evTe_On && (count_seg == ESPERA_30)) {
		            count_seg = 0;
                    HacerTe_Off ();
                    alarma ();
                    state = FINALIZANDO;
                }
                  break;

		case HACIENDO_CAFE:
         		
         		if (evCafe_On && (count_seg < ESPERA_45)) {
                    count_seg++;
                }
                else if (evCafe_On && (count_seg == ESPERA_45)) {
                    count_seg=0;
                    HacerCafe_Off ();
                    alarma ();
                    state = FINALIZANDO;
                }
                break;

		case FINALIZANDO:
                
				if(count_seg < ESPERA_2){
								count_seg++;
						}
						else if (count_seg == ESPERA_2){
								count_seg = 0;
                                evFicha_On = 0;
                                FinAlarma();
                                bienvenido ();
                                Led1Hz ();
								state = REPOSO;
                        }
						break;
		
        } // Cierra Switch Case
        
        hw_Pausems(100);

        } // Cierra While			
    	
    

    hw_DeInit();
    return 0;
    
            
}//finaliza el main



void esperando_orden(void) {
    printf("\nHa ingresado una ficha. Tiempo de espera de seleccion 7 segundos.\nIngrese 2 para CAFE o 3 para TE.\n\n");
    // evFicha_On = 0;
}

void bienvenido (void){
    printf("\nBienvenido. Presione 1 para ingresar ficha.\n\n");
    }

void Led1Hz (void){
    printf("\n\t\t-Indicador Luminoso 1Hz-\n");

}

void Led5Hz (void){
    printf("\n\t\t-Indicador Luminoso 5Hz-\n");

}

void EgresoFicha (void){
    printf("\nSe acabo el tiempo, devolviendo Ficha.");
    }

void HacerTe (void){
    printf("\nHaciendo Te.. Tiempo de espera 7 segundos.\n\n");
    }

void HacerCafe (void){
    printf("\nHaciendo Cafe.. Tiempo de espera 10 segundos.\n\n");
    }

void HacerTe_Off (void){
    printf("\nSe ha finalizado el proceso de Te. Retire su bebida.\nGRACIAS POR ELEGIRNOS\n");
    }

void HacerCafe_Off (void){
    printf("\nSe ha finalizado el proceso de Cafe.GRACIAS POR ELEGIRNOS\n");
    }

void alarma (void){
    printf("\n\t\t-Indicador Sonoro Activado-\n");
}

void FinAlarma (void){
    printf("\n\t\t-Indicador Sonoro Desactivado-\n");
}
